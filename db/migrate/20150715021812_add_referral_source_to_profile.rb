class AddReferralSourceToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :referral_source, :string
  end
end
