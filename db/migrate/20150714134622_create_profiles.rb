class CreateProfiles < ActiveRecord::Migration

  def change
	  drop_table :profiles
    create_table :profiles do |t|
      t.string :company_name
      t.text :company_location
      t.string :contact_person
      t.string :contact_number
      t.string :billing_contact_number
      t.string :billing_contact_person
      t.string :avatar_url
      t.string :member_name
      t.text :address
      t.text :job_titles_held
      t.text :skills
      t.boolean :willing_to_relocate
      t.string :name_of_referral
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
