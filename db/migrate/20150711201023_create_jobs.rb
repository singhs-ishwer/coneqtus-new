class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.string :tagline
      t.text :description
      t.string :position
      t.string :job_type
      t.text :instructions
      t.text :skills
      t.text :prerequisites
      t.string :category
      t.text :location
      t.integer :price
      t.integer :employer_id

      t.timestamps null: false
    end
  end
end
