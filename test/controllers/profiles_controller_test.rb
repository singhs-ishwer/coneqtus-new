require 'test_helper'

class ProfilesControllerTest < ActionController::TestCase
  setup do
    @profile = profiles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:profiles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create profile" do
    assert_difference('Profile.count') do
      post :create, profile: { address: @profile.address, avatar_url: @profile.avatar_url, billing_contact_number: @profile.billing_contact_number, billing_contact_person: @profile.billing_contact_person, company_location: @profile.company_location, company_name: @profile.company_name, contact_number: @profile.contact_number, contact_person: @profile.contact_person, job_titles_held: @profile.job_titles_held, member_name: @profile.member_name, name_of_referral: @profile.name_of_referral, skills: @profile.skills, willing_to_relocate: @profile.willing_to_relocate }
    end

    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should show profile" do
    get :show, id: @profile
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @profile
    assert_response :success
  end

  test "should update profile" do
    patch :update, id: @profile, profile: { address: @profile.address, avatar_url: @profile.avatar_url, billing_contact_number: @profile.billing_contact_number, billing_contact_person: @profile.billing_contact_person, company_location: @profile.company_location, company_name: @profile.company_name, contact_number: @profile.contact_number, contact_person: @profile.contact_person, job_titles_held: @profile.job_titles_held, member_name: @profile.member_name, name_of_referral: @profile.name_of_referral, skills: @profile.skills, willing_to_relocate: @profile.willing_to_relocate }
    assert_redirected_to profile_path(assigns(:profile))
  end

  test "should destroy profile" do
    assert_difference('Profile.count', -1) do
      delete :destroy, id: @profile
    end

    assert_redirected_to profiles_path
  end
end
