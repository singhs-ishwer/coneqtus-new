class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
    before_action :configure_permitted_parameters, if: :devise_controller?
    
protected

def configure_permitted_parameters
  devise_parameter_sanitizer.for(:sign_up) { |u| u.permit({ roles: [] }, :email, :password, :password_confirmation) }
end
   
def ensure_signup_complete
    # Ensure we don't go into an infinite loop
    if current_user && !current_user.profile_complete?
        redirect_to edit_profile_path(current_user), :alert => 'You need to complete your profile before you can view or post jobs.'
    end
end

    # Redirect to the 'finish_signup' page if the user
    # email hasn't been verified yet
  #  if current_user && !current_user.email_verified?
   #   redirect_to finish_signup_path(current_user)
    #end
  #end
end 