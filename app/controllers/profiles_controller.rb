class ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
    before_filter :authenticate_user!

  # GET /profiles
  # GET /profiles.json
  def index
    @profiles = Profile.all
  end

  # GET /profiles/1
  # GET /profiles/1.json
  def show
  end

  # GET /profiles/new
  def new
    @profile = Profile.new
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)

    respond_to do |format|
      if @profile.save
        format.html { redirect_to @profile, notice: 'Profile was successfully created.' }
        format.json { render :show, status: :created, location: @profile }
      else
        format.html { render :new }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    respond_to do |format|
        if  profile_validator
        @profile.update(profile_params)
            format.html { redirect_to @profile, notice: profile_params[:willing_to_relocate].to_s() + 'Profile was suiiiiccessfully updated.' }
        format.json { render :show, status: :ok, location: @profile }
            current_user.profile_complete = true
            current_user.save
      else
            
            format.html {redirect_to edit_profile_path, notice: "Fill all fields" }
        format.json { render json: @profile.errors, status: :unprocessable_entity }
      end
    end
  end

    def profile_validator
        if current_user.roles == 'employee'
            if profile_params[:member_name].blank? ||
                    profile_params[:address].blank? ||
                    profile_params[:job_titles_held].blank? ||
                    profile_params[:skills].blank? 
            return false
            else
                return true
            end
        else if current_user.roles == 'employer'
            if profile_params[:company_name].blank? ||
                    profile_params[:company_location].blank? ||
                    profile_params[:contact_person].blank? ||
                    profile_params[:contact_number].blank? ||
                    profile_params[:billing_contact_number].blank? ||
                    profile_params[:billing_contact_person].blank? 
                return false
            else
                return true
            end
        end
        end
    end
  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html { redirect_to profiles_url, notice: 'Profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_profile
      @profile = Profile.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def profile_params
      params.require(:profile).permit(:company_name, :company_location, :contact_person, :contact_number, :billing_contact_number, :billing_contact_person, :avatar_url, :member_name, :address, :job_titles_held, :skills, :willing_to_relocate, :name_of_referral, :avatar)
    end
end
