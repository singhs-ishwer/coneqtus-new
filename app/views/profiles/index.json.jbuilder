json.array!(@profiles) do |profile|
  json.extract! profile, :id, :company_name, :company_location, :contact_person, :contact_number, :billing_contact_number, :billing_contact_person, :avatar_url, :member_name, :address, :job_titles_held, :skills, :willing_to_relocate, :name_of_referral
  json.url profile_url(profile, format: :json)
end
