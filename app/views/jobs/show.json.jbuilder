json.extract! @job, :id, :title, :tagline, :description, :position, :job_type, :instructions, :skills, :prerequisites, :category, :location, :price, :employer_id, :created_at, :updated_at
